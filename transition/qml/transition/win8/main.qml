import QtQuick 2.0
Rectangle {
    id: root
    width: 360
    height: 480
    color: "black"
    Splash {  }
    ListMenu {
        id: menu
        opacity: 0
        anchors.fill: parent
        anchors.leftMargin: 2*root.width
    }
    SequentialAnimation {
        running: true
        PauseAnimation { duration: 1300 }
        ParallelAnimation {
            NumberAnimation { target: menu; property: "opacity"; to: 1; duration: 600; easing.type: Easing.InOutQuad }
            NumberAnimation { target: menu; property: "anchors.leftMargin"; to: 0; duration: 1000; easing.type: Easing.OutQuint }
            ColorAnimation { target: root; property: "color"; to: "#0e10a0"; duration: 200 }
        }
    }
  //  ProgressBar { anchors.bottom: parent.bottom;width: root.width; x: 0 }
}
